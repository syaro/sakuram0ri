import dgarm from "dgram";
import * as util from "./utils/util";

const receiverServerList = [
  {
    port: 9000,
    address: "10.10.10.2"
  }
];

const socket = dgarm
  .createSocket("udp4")
  .bind({ address: "10.10.10.2", port: 1212 });

const ffplayServer = dgarm.createSocket("udp4");

let ffplay_port: number = 1000;
let ffplay_address: string;

const sequenceNumbers: number[] = [];
setInterval(() => {
  if (sequenceNumbers.length > 500) {
    sequenceNumbers.splice(0, 250);
  }
}, 100);

//from ffplay
ffplayServer.on("message", function(msg, rinfo) {
  ffplay_port = rinfo.port;
  ffplay_address = rinfo.address;
  //send to sender
  Promise.race(
    util.senderServerList.map(el => {
      return util.send(socket, msg, el.address, el.port);
    })
  );
});

//create multi packet receive server
for (const receiveServer of receiverServerList) {
  const server = dgarm.createSocket("udp4");
  server.on("listening", () => {
    const address: any = server.address();
    console.log(`server listening ${address.address}:${address.port}`);
  });
  server.on("message", async function(msg) {
    const seq = util.ipHeader.parse(msg).sequenceNumber;
    if (!sequenceNumbers.some(value => seq === value)) {
      sequenceNumbers.push(seq);
      //send to ffmpeg server
      await util.send(ffplayServer, msg, ffplay_address, ffplay_port);
    }
  });
  server.bind({ port: receiveServer.port, address: receiveServer.address });
}

//connect to ffplay
ffplayServer.bind({
  port: util.FFPLAY_LISTEN_PORT,
  address: util.FFPLAY_LISTEN_ADDRESS
});
