import dgarm from "dgram";
import { Parser } from "binary-parser";

export const FFMPEG_SERVER_ADDRESS = "127.0.0.1";
export const FFMPEG_SERVER_PORT = 1111;
export const FFMPEG_CLIENT_ADDRESS = "127.0.0.1";
export const FFPLAY_LISTEN_ADDRESS = "10.10.10.2";
export const FFPLAY_LISTEN_PORT = 9999;

export const senderServerList = [
  {
    port: 8000,
    address: "172.16.1.2"
  },
  {
    port: 8000,
    address: "172.16.2.2"
  },
  {
    port: 8000,
    address: "172.16.3.2"
  }
];

export const ipHeader = new Parser()
  .endianess("big")
  .uint32("seq")
  .uint32("ack")
  .uint32("sequenceNumber")
  .uint16("windowSize")
  .uint16("checksum")
  .uint32("timestamp");

export function send(
  socket: any,
  msg: Buffer,
  host: string,
  port: number
): Promise<number> {
  return new Promise((resolve, reject) => {
    socket.send(
      msg,
      0,
      msg.length,
      port,
      host,
      (err: string, bytes: number) => {
        if (err) {
          reject(err);
        }
        resolve(bytes);
      }
    );
  });
}
