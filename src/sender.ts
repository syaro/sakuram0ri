import dgarm from "dgram";
import * as util from "./utils/util";

export const sendList = [
  {
    port: 9000,
    address: "10.10.10.2",
  },
  {
    port: 9000,
    address: "10.10.10.2",
  },
  {
    port: 9000,
    address: "10.10.10.2",
  }
];

const sequenceNumbers: number[] = [];
setInterval(() => {
  if (sequenceNumbers.length > 500) {
    sequenceNumbers.splice(0, 250);
  }
}, 100);

const ffmpegServer = dgarm.createSocket("udp4");

// from receiver
for (const senderServer of util.senderServerList) {
  const server = dgarm.createSocket("udp4");
  server.on("listening", () => {
    const address: any = server.address();
    console.log(`server listening ${address.address}:${address.port}`);
  });

  server.on("message", async function(msg) {
    const seq = util.ipHeader.parse(msg).sequenceNumber;

    if (seq && !sequenceNumbers.some(value => seq === value)) {
      sequenceNumbers.push(seq);
      //send to ffmpeg server
      await util.send(
        ffmpegServer,
        msg,
        util.FFMPEG_SERVER_ADDRESS,
        util.FFMPEG_SERVER_PORT
      );
    }
  });

  // from ffmpeg server
  ffmpegServer.on("message", function(msg) {
    //send to receivers
    Promise.race(
      sendList.map(async el => {
        return await util.send(server, msg, el.address, el.port);
      })
    ).catch(err => {
      const address: any = server.address();
      console.log(`${Date.now} ${address.address} disconnected.`)
    });
  });
  server.bind({ port: senderServer.port, address: senderServer.address });
}

ffmpegServer.bind({
  port: 3333,
  address: util.FFMPEG_CLIENT_ADDRESS
});
